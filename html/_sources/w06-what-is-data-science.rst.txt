Week 06 - What is Data Science
==============================

Paradigms of Data Science
-------------------------

We have two major paradigms in Data Science:

.. glossary::

    Hypothesis-Driven Data Science
        We start with a hypothesis or a problem, and we then need to collect or find data for the research.

    Problem-Driven Data Science
        See Hypothesis-Drive Data Science

    Data-Driven Data Science
        We have a data set and want to find new insights from that data.

Data Science is a Process
-------------------------

There are several steps in the Data Science process:

.. figure:: _static/ds-deconstructed.jpeg
    :width: 600px
    :align: center
    :height: 644px
    :alt: The Data Science process
    :figclass: align-center

    The Data Science process


AJ Goldstein does a good job of describing the Data Science process:

`Goldstein, AJ. "Deconstructing Data Science: Breaking The Complex Craft Into It’s Simplest Parts" <https://medium.com/the-mission/deconstructing-data-science-breaking-the-complex-craft-into-its-simplest-parts-15b15420df21>`_

Definitions of Data Science
---------------------------

   *Data science is gaining more and more and widespread attention, but no consensus viewpoint on what data science is has emerged. As a new science, its objects of study and scientific issues should not be covered by established sciences. In the present paper, data science is defined as the science of exploring datanature. We believe this is the most logical and accurate definition of data science (...)*
   – [ZhuXiong2015]_

   *Data Science is an application of scientific methods and principles to data processing.*
   - Henrik Strøm

Ethics
------

No, this is not going to be a social science class, however ...

    *– ethics is important – both in how you conduct your research, and in how you report and communicate it. Data Science is not tabloid journalism or politics.*

With great power comes great responsibility: be honest and follow the evidence!

Framing the problem
-------------------

Framing the problem is the most important part of a Data Science project.
You have to make sure that:

* You are asking a relevant question
* That your question in put into a relevant context
* It is actually possible to answer the question
* You can answer the question with the resources available to you

`Tayo, Benjamin Obi. "Problem Framing: The Most Difficult Stage of a Machine Learning Project Workflow" <https://medium.com/towards-artificial-intelligence/problem-framing-the-most-difficult-stage-of-machine-learning-1a7f208ea414>`_

.. warning:: If you fail at framing the problem, chances are that your project will fail!

.. note:: Do :ref:`Assignment 1<assignment1>`


.. [ZhuXiong2015] Zhu, Yangyong, and Yun Xiong. "Defining data science." arXiv preprint arXiv:1501.05039 (2015).
